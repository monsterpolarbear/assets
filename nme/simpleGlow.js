var nodeMaterial = new BABYLON.NodeMaterial("node");

// InputBlock
var position = new BABYLON.InputBlock("position");
position.visibleInInspector = false;
position.visibleOnFrame = false;
position.target = 1;
position.setAsAttribute("position");

// TransformBlock
var WorldPos = new BABYLON.TransformBlock("WorldPos");
WorldPos.visibleInInspector = false;
WorldPos.visibleOnFrame = false;
WorldPos.target = 1;
WorldPos.complementZ = 0;
WorldPos.complementW = 1;

// InputBlock
var World = new BABYLON.InputBlock("World");
World.visibleInInspector = false;
World.visibleOnFrame = false;
World.target = 1;
World.setAsSystemValue(BABYLON.NodeMaterialSystemValues.World);

// TransformBlock
var WorldPosViewProjectionTransform = new BABYLON.TransformBlock("WorldPos * ViewProjectionTransform");
WorldPosViewProjectionTransform.visibleInInspector = false;
WorldPosViewProjectionTransform.visibleOnFrame = false;
WorldPosViewProjectionTransform.target = 1;
WorldPosViewProjectionTransform.complementZ = 0;
WorldPosViewProjectionTransform.complementW = 1;

// InputBlock
var ViewProjection = new BABYLON.InputBlock("ViewProjection");
ViewProjection.visibleInInspector = false;
ViewProjection.visibleOnFrame = false;
ViewProjection.target = 1;
ViewProjection.setAsSystemValue(BABYLON.NodeMaterialSystemValues.ViewProjection);

// VertexOutputBlock
var VertexOutput = new BABYLON.VertexOutputBlock("VertexOutput");
VertexOutput.visibleInInspector = false;
VertexOutput.visibleOnFrame = false;
VertexOutput.target = 1;

// InputBlock
var Color = new BABYLON.InputBlock("Color3");
Color.visibleInInspector = false;
Color.visibleOnFrame = false;
Color.target = 1;
Color.value = new BABYLON.Color3(0.4549019607843137, 0.6980392156862745, 0.8549019607843137);
Color.isConstant = false;

// ColorMergerBlock
var ColorMerger = new BABYLON.ColorMergerBlock("ColorMerger");
ColorMerger.visibleInInspector = false;
ColorMerger.visibleOnFrame = false;
ColorMerger.target = 4;

// RemapBlock
var Remap = new BABYLON.RemapBlock("Remap");
Remap.visibleInInspector = false;
Remap.visibleOnFrame = false;
Remap.target = 4;
Remap.sourceRange = new BABYLON.Vector2(-1, 1);
Remap.targetRange = new BABYLON.Vector2(0, 1);

// MultiplyBlock
var Multiply = new BABYLON.MultiplyBlock("Multiply");
Multiply.visibleInInspector = false;
Multiply.visibleOnFrame = false;
Multiply.target = 4;

// TrigonometryBlock
var Sin = new BABYLON.TrigonometryBlock("Sin");
Sin.visibleInInspector = false;
Sin.visibleOnFrame = false;
Sin.target = 4;
Sin.operation = BABYLON.TrigonometryBlockOperations.Sin;

// DivideBlock
var Divide = new BABYLON.DivideBlock("Divide");
Divide.visibleInInspector = false;
Divide.visibleOnFrame = false;
Divide.target = 4;

// SubtractBlock
var Subtract = new BABYLON.SubtractBlock("Subtract");
Subtract.visibleInInspector = false;
Subtract.visibleOnFrame = false;
Subtract.target = 4;

// InputBlock
var Float = new BABYLON.InputBlock("Float");
Float.visibleInInspector = false;
Float.visibleOnFrame = false;
Float.target = 1;
Float.value = 1;
Float.min = 0;
Float.max = 0;
Float.isBoolean = false;
Float.matrixMode = 0;
Float.animationType = BABYLON.AnimatedInputBlockTypes.None;
Float.isConstant = false;

// MultiplyBlock
var Multiply1 = new BABYLON.MultiplyBlock("Multiply");
Multiply1.visibleInInspector = false;
Multiply1.visibleOnFrame = false;
Multiply1.target = 4;

// InputBlock
var Time = new BABYLON.InputBlock("Time");
Time.visibleInInspector = false;
Time.visibleOnFrame = false;
Time.target = 1;
Time.value = 0;
Time.min = 0;
Time.max = 0;
Time.isBoolean = false;
Time.matrixMode = 0;
Time.animationType = BABYLON.AnimatedInputBlockTypes.Time;
Time.isConstant = false;

// InputBlock
var Speed = new BABYLON.InputBlock("Speed");
Speed.visibleInInspector = false;
Speed.visibleOnFrame = false;
Speed.target = 1;
Speed.value = 5;
Speed.min = 0;
Speed.max = 0;
Speed.isBoolean = false;
Speed.matrixMode = 0;
Speed.animationType = BABYLON.AnimatedInputBlockTypes.None;
Speed.isConstant = false;

// InputBlock
var Wavelength = new BABYLON.InputBlock("Wavelength");
Wavelength.visibleInInspector = false;
Wavelength.visibleOnFrame = false;
Wavelength.target = 1;
Wavelength.value = 1.235;
Wavelength.min = 0;
Wavelength.max = 0;
Wavelength.isBoolean = false;
Wavelength.matrixMode = 0;
Wavelength.animationType = BABYLON.AnimatedInputBlockTypes.None;
Wavelength.isConstant = false;

// InputBlock
var Amplitude = new BABYLON.InputBlock("Amplitude");
Amplitude.visibleInInspector = false;
Amplitude.visibleOnFrame = false;
Amplitude.target = 1;
Amplitude.value = 1;
Amplitude.min = 0;
Amplitude.max = 0;
Amplitude.isBoolean = false;
Amplitude.matrixMode = 0;
Amplitude.animationType = BABYLON.AnimatedInputBlockTypes.None;
Amplitude.isConstant = false;

// NegateBlock
var Negate = new BABYLON.NegateBlock("Negate");
Negate.visibleInInspector = false;
Negate.visibleOnFrame = false;
Negate.target = 4;

// InputBlock
var Float1 = new BABYLON.InputBlock("Float");
Float1.visibleInInspector = false;
Float1.visibleOnFrame = false;
Float1.target = 1;
Float1.value = 0.25;
Float1.min = 0;
Float1.max = 0;
Float1.isBoolean = false;
Float1.matrixMode = 0;
Float1.animationType = BABYLON.AnimatedInputBlockTypes.None;
Float1.isConstant = false;

// InputBlock
var Float2 = new BABYLON.InputBlock("Float");
Float2.visibleInInspector = false;
Float2.visibleOnFrame = false;
Float2.target = 1;
Float2.value = 1;
Float2.min = 0;
Float2.max = 0;
Float2.isBoolean = false;
Float2.matrixMode = 0;
Float2.animationType = BABYLON.AnimatedInputBlockTypes.None;
Float2.isConstant = false;

// FragmentOutputBlock
var FragmentOutput = new BABYLON.FragmentOutputBlock("FragmentOutput");
FragmentOutput.visibleInInspector = false;
FragmentOutput.visibleOnFrame = false;
FragmentOutput.target = 2;
FragmentOutput.convertToGammaSpace = false;
FragmentOutput.convertToLinearSpace = false;

// Connections
position.output.connectTo(WorldPos.vector);
World.output.connectTo(WorldPos.transform);
WorldPos.output.connectTo(WorldPosViewProjectionTransform.vector);
ViewProjection.output.connectTo(WorldPosViewProjectionTransform.transform);
WorldPosViewProjectionTransform.output.connectTo(VertexOutput.vector);
Color.output.connectTo(ColorMerger.rgbIn);
Float.output.connectTo(Subtract.left);
Time.output.connectTo(Multiply1.left);
Speed.output.connectTo(Multiply1.right);
Multiply1.output.connectTo(Subtract.right);
Subtract.output.connectTo(Divide.left);
Wavelength.output.connectTo(Divide.right);
Divide.output.connectTo(Sin.input);
Sin.output.connectTo(Multiply.left);
Amplitude.output.connectTo(Multiply.right);
Multiply.output.connectTo(Remap.input);
Amplitude.output.connectTo(Negate.value);
Negate.output.connectTo(Remap.sourceMin);
Amplitude.output.connectTo(Remap.sourceMax);
Float1.output.connectTo(Remap.targetMin);
Float2.output.connectTo(Remap.targetMax);
Remap.output.connectTo(ColorMerger.a);
ColorMerger.rgba.connectTo(FragmentOutput.rgba);

// Output nodes
nodeMaterial.addOutputNode(VertexOutput);
nodeMaterial.addOutputNode(FragmentOutput);
nodeMaterial.build();