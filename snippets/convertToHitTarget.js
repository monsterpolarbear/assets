// For touch events in Babylon.js PEP is required. 
// For Highlights, make sure engine was created with stencil on:
// var engine = new BABYLON.Engine(canvas, true, { stencil: true });



var createScene = function() {
    var scene = new BABYLON.Scene(engine);


    var camera = new BABYLON.ArcRotateCamera("Camera", 0, 0, 10, new BABYLON.Vector3(0, 0, 0), scene);
    camera.setPosition(new BABYLON.Vector3(0, 0, 20));
    camera.attachControl(canvas, true);
    var light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 0.7;



    BABYLON.SceneLoader.AppendAsync("https://monsterpolarbear.gitlab.io/assets/", "twoParts.glb", scene)
        .then((res) => {

            // Highlight several objects red
            const objA = scene.getMeshByName('Cube.001');
            const objB = scene.getMeshByName('Cylinder');


            scene.onPointerObservable.add((pointerInfo) => {
                switch (pointerInfo.type) {
                    case BABYLON.PointerEventTypes.POINTERPICK:
                        let pickResult = pointerInfo.pickInfo;
                        let name = pickResult.pickedMesh.name;
                        console.log(name);
                        break;
                }
            })
        })







    scene.debugLayer.show({
        embedMode: true,
    });



    return scene;
};