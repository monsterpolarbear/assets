// * boxes/cylinders as hit targets? 
// * or convert to buttons, should stay same screen size?
// * on click instance, then instance as hit target for above


// * support hierarchies
// * parts hierarchies
// * instance and parent
// * track/save copies to dispose later
// * copy animations
// * remove instanced/copied hierarchies, dispose
// * remove animations



var createScene = function() {
    // This creates a basic Babylon Scene object (non-mesh)
    var scene = new BABYLON.Scene(engine);


    var camera = new BABYLON.ArcRotateCamera("Camera", 0, 0, 10, new BABYLON.Vector3(0, 0, 0), scene);
    camera.setPosition(new BABYLON.Vector3(0, 0, 20));
    camera.attachControl(canvas, true);


    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    var light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 0.7;


    BABYLON.SceneLoader.AppendAsync("https://monsterpolarbear.gitlab.io/assets/", "twoParts.glb", scene)
        .then((res) => {





            // Instancing
            const parentNode = scene.getTransformNodeByName("Bone.001");

            const partRootNode = scene.getTransformNodeByName("part2");

            // We need to track 
            //  * original
            //  * parent
            //  * descendants
            //  * animations
            //  * animationgroup
            const instancedPartRootNode = partRootNode.instantiateHierarchy(parentNode);

            // see https://forum.babylonjs.com/t/loading-character-with-animations-to-scene-and-cloning-it-with-animation-groups/19685/5
            // https://playground.babylonjs.com/#IJ5DL6#4
            // get descendants of instance
            const originalDescendants = partRootNode.getDescendants(false);
            const instanceDescendants = instancedPartRootNode.getDescendants(false);

            console.log(originalDescendants);
            console.log(instanceDescendants);


            // clone an animationgroup and set new target
            const part2Anim = scene.getAnimationGroupByName("part2_Move");

            const cloneBone = scene.getTransformNodeByName("Clone of Bone");
            let newAnimGroup = part2Anim.clone("animClone", (oldTarget) => { return cloneBone })
            newAnimGroup.start(1);


            // run animation
            //part2Anim.start(1);

            window.setTimeout(function() {

                console.log("Disposing Animation Group")
                newAnimGroup.dispose();
            }, 2000);

        })



    scene.debugLayer.show({
        embedMode: true,
    });

    return scene;
};